import java.util.BitSet;

/**
 * Created by Tonydouche on 25/11/16.
 */
public class DiningCryptographers {

    public static void main(String[] args) {
        DiningCryptographers dc = new DiningCryptographers();
        // SA, DA, SB, DB, M, B
        dc.sendSecret(0x0C73, 0xA2A9, 0x80C1, 0x92F5, 0x9B57, "0");
        dc.sendSecret(0x27C2, 0x35F6, 0x0879, 0x1A4D, 0x27BC, "1");
        dc.sendSecret(0xBF0D, 0x186F, 0x3C99, 0x2EAD, 0x62AB, "0");
        dc.sendSecret(0x9275, 0x8041, 0xCA59, 0xD86D, 0x3A92, "1");
        dc.sendSecret(0x22D9, 0x30ED, 0x7BEB, 0x69DF, 0x6451, "1");
    }

    //if b == 0 send secret along with message, 0000 if no anonymous message
    public static void sendSecret(int SAs, int DAs, int SBs, int DBs, int Ms, String bs) {
        BitSet SA = hexToBin(SAs);
        BitSet DA = hexToBin(DAs);
        BitSet SB = hexToBin(SBs);
        BitSet DB = hexToBin(DBs);
        BitSet M = hexToBin(Ms);

        SA.xor(SB);

        if (bs.equals("0")) {
            BitSet SAxorSB = (BitSet) SA.clone();
            SA.xor(DA);
            SA.xor(DB);
            System.out.println(binToHex(SAxorSB).toUpperCase() + binToHex(SA).toUpperCase());
            return;
        } else if (bs.equals("1")) {
            SA.xor(M);
            System.out.println(binToHex(SA).toUpperCase());
            return;
        }

    }

    public static BitSet hexToBin(int hex) {
        BitSet temp = new BitSet(16);
        String binary = Integer.toBinaryString(hex);
        int size = binary.length();
        if (size < 16) {
            for (int i = 0; i < 16 - size; i++) {
                binary = "0" + binary;
            }
        }
        for (int j = 0; j < 16; j++) {
            if (binary.charAt(j) == '1') {
                temp.set(j, true);
            } else if (binary.charAt(j) == '0') {
                temp.set(j, false);
            }
        }
        return temp;
    }

    public static String binToHex(BitSet bs) {
        String binary = "";
        for (int i = 0; i < 16; i++) {
            if (bs.get(i)) {
                //binary = "1" + binary;
                binary += "1";
            } else if (!bs.get(i)) {
                //binary = "0" + binary;
                binary += "0";

            }
        }
        return  String.format("%4X", Long.parseLong(binary,2)).replace(' ', '0');
    }
}
