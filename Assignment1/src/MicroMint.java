import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by Tonydouche on 14/11/16.
 */
public class MicroMint {
    private static Random random;
    private static HashMap<Integer, Integer> map;
    private static double lamda = 3.66;

    public static void main(String[] args) {
        MicroMint mm = new MicroMint();
        mm.run();
    }

    public MicroMint() {
        random = new Random();
        map = new HashMap<>();
    }

    private void run() {
        ArrayList throwList = new ArrayList<>();
        int currentWidth = Integer.MAX_VALUE;
        System.out.println("#coins(c) mean (x) c.i width throws");
        System.out.println("First example");
        System.out.println("checks current width == desired width");
        int thrown = 0;
        while (true) {
            thrown++;
            throwList.add(Algorithm(16, 2, 1));
            currentWidth = confidenceInterval2(throwList);
            if (22 == currentWidth) {
                System.out.println("    1        " + mean(throwList) + "        " + currentWidth +"    " + thrown);
                break;
            }
        }
        thrown = 0;
        throwList.clear();
        while (true) {
            thrown++;
            throwList.add(Algorithm(16, 2, 100));
            currentWidth = confidenceInterval2(throwList);
            if (24 == currentWidth) {
                System.out.println("    100      " + mean(throwList) + "       " + currentWidth +"    " + thrown);
                break;
            }
        }
        thrown = 0;
        throwList.clear();
        while (true) {
            thrown++;
            throwList.add(Algorithm(16, 2, 10000));
            currentWidth = confidenceInterval2(throwList);
            if (22 == currentWidth) {
                System.out.println("    10000    " + mean(throwList) + "      " + currentWidth +"    " + thrown);
                break;
            }
        }
        System.out.println("Second example");
        System.out.println("checks currentWidth <= desired width");
        thrown = 0;
        throwList.clear();
        while (true) {
            thrown++;
            throwList.add(Algorithm(20, 7, 1));
            currentWidth = confidenceInterval2(throwList);
            if (0 < currentWidth && currentWidth <= 79671) {
                System.out.println("    1    " + mean(throwList) + "      " + currentWidth +"    " + thrown);
                break;
            }
        }
        thrown = 0;
        throwList.clear();
        while (true) {
            thrown++;
            throwList.add(Algorithm(20, 7, 100));
            currentWidth = confidenceInterval2(throwList);
            if (0 < currentWidth && currentWidth <= 15616) {
                System.out.println("    100    " + mean(throwList) + "      " + currentWidth +"    " + thrown);
                break;
            }
        }
        thrown = 0;
        throwList.clear();
        while (true) {
            thrown++;
            throwList.add(Algorithm(20, 7, 10000));
            currentWidth = confidenceInterval2(throwList);
            //if (4400 < currentWidth && currentWidth < 5000) {
            if (0 < currentWidth  && currentWidth <= 4783) {
                System.out.println("    10000    " + mean(throwList) + "      " + currentWidth +"    " + thrown);
                break;
            }
        }
    }

    private static int confidenceInterval2 (ArrayList<Integer> list) {
        int x = 0;
        for (int i = 0; i < list.size(); i++) {
            x += list.get(i);
        }
        x /= list.size();
        long temp = 0;
        long sum = 0;
        for (int j = 0; j < list.size(); j++) {
            temp = (list.get(j)-x)*(list.get(j)-x);
            sum += temp;
        }
        int s = (int) Math.sqrt((sum/list.size()));
        double conf = s * lamda / Math.sqrt(list.size());
        int width = (int) conf;
        return 2*width;
    }

    private static int mean(ArrayList<Integer> throwList) {
        int sum = 0;
        for (int i = 0; i < throwList.size(); i++) {
            sum += throwList.get(i);
        }
        if (throwList.isEmpty()) {
            return 0;
        } else {
            return sum / throwList.size();
        }
    }

    //paramters u (b=2^u bins), k(balls in same bin to make coin), c(number of coins produced).
    public static int Algorithm(int u, int k, int c) {
        map.clear();
        int b = (int) Math.pow(2, u);
        int thrown = 0;
        int bag = 0;
        int created = 0;
        int temp;
        while (created < c) {
            thrown++;
            bag = random.nextInt(b);
            if (map.containsKey(bag)) {
                temp = map.get(bag);
                temp++;
                map.put(bag, temp);
                if (temp == k) {
                    created++;
                }
            } else {
                map.put(bag, 1);
            }
        }
        return thrown;
    }
}
