import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Tonydouche on 10/11/16.
 */
public class LuhnAlgorithm {

    private String[][] cardList;
    private File file;
    private String path;

    public static void main(String[] args) {
        //requires the user to create a file with numbers to be tested and pass the file name on creation of LuhnAlgorithm
        LuhnAlgorithm lalgo = new LuhnAlgorithm("testFile");
        lalgo.readFromFile();
        lalgo.computeSum();
    }

    public LuhnAlgorithm(String path) {
        this.cardList = new String[100][16];
        this.path = path;
        file = new File(path);
    }

    public void readFromFile() {
        FileInputStream fstream = null;
        try {
            fstream = new FileInputStream("testFile");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
        String line;
        try {
            int counter = 0;
            while ((line = br.readLine()) != null) {
                String[] nbrs = line.split("");
                for (int i = 0; i < nbrs.length; i++) {
                    cardList[counter][i] = nbrs[i];
                }
                counter++;
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void computeSum() {
        StringBuilder sb = new StringBuilder();
        int tempResult;
        int tempSum;
        int temp;
        boolean doubledX;
        String tempNbr;
        for (int i = 0; i < 100; i++) {
            tempSum = 0;
            doubledX = false;
            for (int j = 0; j < 16; j++) {
                tempNbr = cardList[i][j];
                if (tempNbr.compareTo("X") != 0) {
                    if (j % 2 == 0) {
                        temp = (2 * Integer.valueOf(tempNbr));
                        if (temp > 9) {
                            temp -= 9;
                        }
                        tempSum += temp;
                    } else {
                        tempSum += Integer.valueOf(tempNbr);
                    }
                } else {
                    doubledX = j % 2 == 0;
                }
            }
            tempResult = computeX(tempSum, doubledX);
            sb.append(String.valueOf(tempResult));
        }
        for (int r = 0; r < sb.length(); r++) {
            System.out.print(sb.charAt(r));
        }
    }

    public int computeX(int sum, boolean doubled) {
        int tempSum = sum;
        tempSum /= 10;
        tempSum++;
        tempSum *= 10;
        int i = tempSum - sum;
        if (i == 0 || i == 10) {
            return 0;
        }
        if (doubled) {
            if (i == 1) {
                return 5;
            }
            if (i == 3) {
                return 6;
            }
            if (i == 2 || i == 4 || i == 6 || i == 8) {
                return i / 2;
            }
            if (i == 5) {
                return 7;
            }
            if (i == 7) {
                return 8;
            }
            if (i == 9) {
                return 9;
            }
        } else {
            return i;
        }
        return 1337;
    }
}
