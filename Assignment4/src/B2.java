import java.io.*;
import java.math.BigInteger;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * Created by Tonydouche on 08/12/16.
 */
public class B2 {

    private static BigInteger p, g, gx1, x2, gx2, DHss;

    public static void main(String[] args) {

        Random rand = new Random();

        String url = "eitn41.eit.lth.se";
        int port = 1337;

        PrintWriter out = null;
        BufferedReader in = null;
        try {
            Socket socket = new Socket(url, port);
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String hexInputForP = "FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD1" +
                "29024E088A67CC74020BBEA63B139B22514A08798E3404DD" +
                "EF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245" +
                "E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7ED" +
                "EE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3D" +
                "C2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F" +
                "83655D23DCA3AD961C62F356208552BB9ED529077096966D" +
                "670C354E4ABC9804F1746C08CA237327FFFFFFFFFFFFFFFF";
        //converts hexstring to decimal, interpreting with 16-base (hexa)
        p = new BigInteger(hexInputForP, 16);
        g = BigInteger.valueOf(2);

        try {
            // Bob receives gx1 from Alice
            gx1 = new BigInteger(in.readLine(), 16);

            // computes gx2 and send
            x2 = BigInteger.valueOf(rand.nextInt(444)+444);
            gx2 = g.modPow(x2, p);
            out.println(gx2.toString(16));

            String str = in.readLine();
            // if ack, compute shared secret (DHss)
            if (str.equals("ack")) {
                DHss = gx1.modPow(x2, p);

                //continue with socialist millionaire
                //read ga2 from Alice
                BigInteger ga2 = new BigInteger(in.readLine(), 16);
                //pick random b2, and compute gb2, send to Alice
                BigInteger b2 = BigInteger.valueOf(rand.nextInt(444)+444);
                BigInteger gb2 = g.modPow(b2, p);
                out.println(gb2.toString(16));
                String str2 = in.readLine();
                //receive ack/nak from Alice
                if (str2.equals("ack")) {
                    //receive ga3 from Alice
                    BigInteger ga3 = new BigInteger(in.readLine(), 16);
                    //pick random b3, and compute gb3, send to Alice
                    BigInteger b3 = BigInteger.valueOf(rand.nextInt(444)+444);
                    BigInteger gb3 = g.modPow(b3, p);
                    out.println(gb3.toString(16));

                    //receive ack/nak from Alice
                    String str3 = in.readLine();
                    if (str3.equals("ack")) {
                        //receive Pa from Alice
                        BigInteger Pa = new BigInteger(in.readLine(), 16);

                        //pick random b, compute Pb, send to Alice
                        BigInteger b = BigInteger.valueOf(rand.nextInt(444)+444);
                        BigInteger g3 = ga3.modPow(b3, p);
                        BigInteger Pb = g3.modPow(b, p);
                        out.println(Pb.toString(16));

                        //receive ack/nak from Alice
                        String str4 = in.readLine();
                        if (str4.equals("ack")) {
                            //receive Qa from Alice
                            BigInteger Qa = new BigInteger(in.readLine(), 16);

                            //find y, compute Qb, send to Alice
                            String sharedPassphrase = "eitn41 <3";
                            BigInteger y = computeSharedSecret(DHss, sharedPassphrase);

                            BigInteger g2 = ga2.modPow(b2, p);
                            BigInteger Qb = g.modPow(b,p).multiply(g2.modPow(y, p));
                            Qb = Qb.mod(p);
                            out.println(Qb.toString(16));

                            //receive ack/nak from Alice
                            String str5 = in.readLine();
                            if (str5.equals("ack")) {
                                //receive Ra == (QaQb^-1)^a3 from Alice
                                BigInteger Ra = new BigInteger(in.readLine(), 16);

                                //compute Rb, send to Alice
                                BigInteger Rb = Qa.multiply(Qb.modInverse(p)).modPow(b3, p);
                                out.println(Rb.toString(16));

                                String str6 = in.readLine();
                                if (str6.equals("ack")) {
                                    String authentication = in.readLine();
                                    System.out.println("aunthentication: " + authentication);

                                    String msg = "0123456789abcdef";
                                    //Encrypt message and send
                                    BigInteger encryptedMessage = new BigInteger(msg, 16).xor(DHss);
                                    out.println(encryptedMessage.toString(16));

                                    //Reply from Alice
                                    System.out.println("Response message: " + in.readLine());
                                } else {
                                    System.exit(1);
                                }
                            } else {
                                System.exit(1);
                            }
                        } else {
                            System.exit(1);
                        }
                    } else {
                        System.exit(1);
                    }
                } else {
                    System.exit(1);
                }
            } else {
                System.exit(1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //x = y = H(PKA || PKB || g^x1y1 || ”shared secret”)
    // g^x1y1 == gx1^x2 mod(p) == DHss
    public static BigInteger computeSharedSecret(BigInteger DHss, String sharedPP){
        byte[] DH = DHss.toByteArray();
        byte[] DHTrim;
        if (DH[0] == 0x00) {
            DHTrim = new byte[DH.length - 1];
            System.arraycopy(DH, 1, DHTrim, 0, DHTrim.length);
        } else {
            DHTrim = new byte[DH.length];
            System.arraycopy(DH, 0, DHTrim, 0, DHTrim.length);
        }

        byte[] Gxy = null;
        try {
            Gxy = sharedPP.getBytes("UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte[] conc = new byte[DHTrim.length+Gxy.length];
        System.arraycopy(DHTrim, 0, conc, 0, DHTrim.length);
        System.arraycopy(Gxy, 0, conc, DHTrim.length, Gxy.length);

        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update(conc);
        return new BigInteger(1, md.digest());
    }
}
