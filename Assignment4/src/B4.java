import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * Created by Tonydouche on 09/12/16.
 */
public class B4 {

    private static MessageDigest md;
    private static B3 b3;
    private static Random rand;

    public static void main(String[] args) {
        try {
            md = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        b3 = new B3();
        rand = new Random();

        String M = "c107782954829b34dc531c14b40e9ea482578f988b719497aa0687";
        String S = "1e652ec152d0bfcd65190ffc604c0933d0423381";

        try {
            String EM = OAEP_encode(M, S);
            System.out.println("EM is: ");
            System.out.println(EM);
            System.out.println("");
            String EMtest = "0063b462be5e84d382c86eb6725f70e59cd12c0060f9d3778a18b7aa067f90b2" +
                    "178406fa1e1bf77f03f86629dd5607d11b9961707736c2d16e7c668b367890bc" +
                    "6ef1745396404ba7832b1cdfb0388ef601947fc0aff1fd2dcd279dabde9b10bf" +
                    "c51efc06d40d25f96bd0f4c5d88f32c7d33dbc20f8a528b77f0c16a7b4dcdd8f";
            System.out.println("DM is: ");
            String DM = OAEP_decode(EMtest);
            System.out.println(DM);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // input: message M to be encoded, and the seed S
    // output: encoded message EM
    // hexa input, output in hexa
    // encoded message 128 bytes, preparing for 1024 bit RSA
    public static String OAEP_encode(String M, String S) throws Exception {
        String L = "";
        int k = 128;
        int hLen = 160/8; // 20 octets, since SHA-1 yields 160-bits
        //int mLen = M.length();
        byte[] mArray = hexStringToByteArray(M);
        int mLen = mArray.length;

        if (mLen > k - 2*hLen - 2) {
            System.out.println("message too long");
            System.exit(1);
        }

        byte[] LArray = hexStringToByteArray(L);
        md.update(LArray);
        byte[] lhash = md.digest();
        int padding = k - mLen - 2*hLen - 2;
        byte[] PS = new byte[padding];
        byte[] hexValue = {0x01};
        byte[] DB = new byte[k-hLen-1];

        System.arraycopy(lhash, 0, DB, 0, lhash.length);
        System.arraycopy(PS, 0, DB, lhash.length, PS.length);
        System.arraycopy(hexValue, 0, DB, lhash.length+PS.length, 1);
        System.arraycopy(mArray, 0, DB, lhash.length+PS.length+1,mArray.length);

        byte[] dbMask = hexStringToByteArray(b3.MGF1(S, k-hLen-1));
        byte[] maskedDB = new byte[DB.length];
        for (int i=0;i<DB.length;i++) {
            int temp = DB[i] ^ dbMask[i];
            byte b = (byte) temp;
            maskedDB[i] = b;
        }
        byte[] seedMask = hexStringToByteArray(b3.MGF1(bytesToHex(maskedDB), hLen));
        byte[] maskedSeed = new byte[seedMask.length];
        byte[] seed = hexStringToByteArray(S);

        for (int i=0;i<seedMask.length;i++) {
            int temp = seedMask[i] ^ seed[i];
            byte b = (byte) temp;
            maskedSeed[i] = b;
        }
        byte[] hexValue2 = {0x00};
        byte[] EM = new byte[1+maskedSeed.length+maskedDB.length];

        System.arraycopy(hexValue2, 0, EM, 0, hexValue2.length);
        System.arraycopy(maskedSeed, 0, EM, hexValue2.length, maskedSeed.length);
        System.arraycopy(maskedDB, 0, EM, 1+maskedSeed.length, maskedDB.length);

        return bytesToHex(EM);
    }

    // input: encoded message EM
    // output: decoded message M
    public static String OAEP_decode(String EM) throws  Exception{
        String L = "";
        int k = 128;
        int hLen = 160/8; // 20 octets, since SHA-1 yields 160-bits
        byte[] emArray = hexStringToByteArray(EM);
        int mLen = emArray.length;
        if (mLen != k) {
            System.out.println("decryption error");
            System.exit(1);
        }
        if (k < 2*hLen + 2) {
            System.out.println("decryption error");
            System.exit(1);
        }
        byte[] LArray = hexStringToByteArray(L);
        md.update(LArray);
        byte[] lhash = md.digest();
        byte Y = emArray[0];
        if ( Y != 0x00) {
            System.out.println("decryption error");
            System.exit(1);
        }
        byte[] maskedSeed = new byte[hLen];
        System.arraycopy(emArray,1,maskedSeed,0,hLen);
        byte[] maskedDB = new byte[k-hLen-1];
        System.arraycopy(emArray,1+hLen,maskedDB,0,k-hLen-1);
        byte[] seedMask = hexStringToByteArray(b3.MGF1(bytesToHex(maskedDB), hLen));
        byte[] seed = new byte[seedMask.length];

        for (int i=0;i<seed.length;i++) {
            int temp =  maskedSeed[i] ^ seedMask[i];
            byte b = (byte) temp;
            seed[i] = b;
        }
        byte[] dbMask = hexStringToByteArray(b3.MGF1(bytesToHex(seed), k - hLen -1));
        byte[] DB = new byte[dbMask.length];
        for (int i=0;i<DB.length;i++) {
            int temp =  maskedDB[i] ^ dbMask[i];
            byte b = (byte) temp;
            DB[i] = b;
        }
        byte[] lhash2 = new byte[hLen];
        System.arraycopy(DB, 0,lhash2,0,hLen);

        boolean equal;
        for (int i=0;i<lhash2.length;i++) {
            equal = lhash[i] == lhash2[i];
            if(!equal) {
                System.out.println("decryption error");
                System.exit(1);
            }
        }
        boolean found = false;
        int pos;
        for (pos = hLen;pos<DB.length;pos++) {
            if (DB[pos] == 0x01) {
                found = true;
                break;
            }
        }
        if (!found) {
            System.out.println("decryption error");
            System.exit(1);
        }
        byte[] M = new byte[DB.length-pos-1];
        System.arraycopy(DB, pos+1, M, 0, DB.length-pos-1);
        return bytesToHex(M);
    }

    // professional google search...seems legit
    final protected static char[] hexComparison = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexComparison[v >>> 4];
            hexChars[j * 2 + 1] = hexComparison[v & 0x0F];
        }
        return new String(hexChars);
    }

    //another fine craft made by someone else, looks legit
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
}
