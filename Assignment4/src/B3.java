import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Tonydouche on 09/12/16.
 */
public class B3 {
    private static MessageDigest md;
    private static String mgfSeed = "9b4bdfb2c796f1c16d0c0772a5848b67457e87891dbc8214";
    private static int maskLen = 21;

    public static void main(String[] args) {
        try {
            String result = MGF1(mgfSeed, maskLen);
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //MGF1 is a mask generation function based on a hash function.
    //mgfSeed:  seed from which mask is generated, an octet string
    //maskLen:  intended length in octets of the mask, at most 2^32 hLen
    public static String MGF1(String mgfSeed, int maskLen) throws Exception {
        try {
            //produces 160-bit message digest (20 octets)
            md = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        int hLen = 20;
        // limit = 2^32
        int limit = (int)Math.pow(2, 32);
        if (limit < maskLen) {
            System.out.println("Mask too long");
            System.exit(1);
        }
        int ceil = (int) Math.ceil((double) (maskLen/hLen));
        byte[] mgf = hexStringToByteArray(mgfSeed);
        byte[] C;
        byte[] mask = new byte[maskLen];
        byte[] result;

        int counter;
        for (counter=0;counter<ceil;counter++) {
            md.update(mgf);
            C = I2OSP(counter, 4);
            md.update(C);
            result = md.digest();
            System.arraycopy(result, 0, mask, counter*hLen, hLen);
        }
        // FFS! must pad the space that can occur!
        if((counter * hLen) < maskLen) {
            C = I2OSP(counter, 4);
            md.update(mgf);
            md.update(C);
            result = md.digest();

            System.arraycopy(result, 0, mask, counter*hLen, mask.length - (counter*hLen));
        }
        return bytesToHex(mask);
    }
    //another fine craft made by someone else, looks legit
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    //I2OSP converts a non-negative integer to an octet string of a specified length.
    public static byte[] I2OSP(int x, int xLen) {
        int limit = (int) Math.pow(256, xLen);
        if (limit < x) {
            System.out.print("integer too large");
            System.exit(1);
        }
        byte[] i20sp = new byte[xLen];
        for (int i = xLen - 1; 0 <= i; i--) {
            i20sp[i] = (byte) (x >>> (8 * (xLen - 1 - i)));
        }
        return i20sp;
    }

    // professional google search...seems legit
    final protected static char[] hexComparison = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexComparison[v >>> 4];
            hexChars[j * 2 + 1] = hexComparison[v & 0x0F];
        }
        return new String(hexChars);
    }
}
