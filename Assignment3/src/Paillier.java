import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;

/**
 * Created by Tonydouche on 05/12/16.
 */
public class Paillier {

    private static BigInteger p, q, n, L, x, lambda, g, u, c, sum, taljare, namnare, result;

    public static void main(String[] args) {
        sum = BigInteger.ONE;
        p = new BigInteger("1117");
        q = new BigInteger("1471");
        n = p.multiply(q);
        g = new BigInteger("652534095028");

        taljare = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
        namnare = p.subtract(BigInteger.ONE).gcd(q.subtract(BigInteger.ONE));
        lambda = taljare.divide(namnare);

        x = g.pow(lambda.intValue()).mod(n.pow(2));
        L = x.subtract(BigInteger.ONE).divide(n);
        u = L.modInverse(n);

        countVotes();
    }
    private static void countVotes() {
        BufferedReader br = null;
        int counter = 0;
        try {
            br = new BufferedReader(new FileReader("votes"));
            String vote;
            while((vote = br.readLine()) != null) {
                c = new BigInteger(vote);
                sum = sum.multiply(c);
                counter++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        sum = sum.mod(n.pow(2));
        L = sum.pow(lambda.intValue()).mod(n.pow(2)).subtract(BigInteger.ONE).divide(n);
        result = L.multiply(u).mod(n);
        if (result.compareTo(BigInteger.valueOf(counter)) < 1) {
            System.out.println(result);
        } else {
            result = result.subtract(n);
            System.out.println(result);
        }
    }
}
