import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * Created by Tonydouche on 05/12/16.
 */
public class Treshhold {

    private static BigDecimal[] goodies = {BigDecimal.valueOf(1), BigDecimal.valueOf(4), BigDecimal.valueOf(5), BigDecimal.valueOf(6)};
    //the first value in yj is calculated from the given polynomial by setting x to 1.
    private static BigDecimal[] yj = {BigDecimal.valueOf(244), BigDecimal.valueOf(5815), BigDecimal.valueOf(10792), BigDecimal.valueOf(18049)};
    private static BigDecimal[] lj = new BigDecimal[yj.length];

    public static void main(String[] args) {
        //double sum = 1;
        BigDecimal sum = BigDecimal.ONE;
        for (int i = 0; i < goodies.length; i++) {
            for (int j = 0; j < goodies.length; j++) {
                if (i != j) {
                    sum = sum.multiply(BigDecimal.valueOf(-1));
                    sum = sum.multiply(goodies[j]);
                    sum = sum.divide(goodies[i].subtract(goodies[j]), MathContext.DECIMAL32);
                    //sum = sum.multiply(BigDecimal.valueOf(-1)).multiply(goodies[j]).divide(goodies[i].subtract(goodies[j]));
                    //sum *=(double) (-1) * goodies2[j] / (goodies2[i] - goodies2[j]);
                }
            }
            lj[i] = sum;
            sum = BigDecimal.ONE;
        }
        BigDecimal result = BigDecimal.ZERO;
        for (int i=0; i<lj.length;i++) {
            result =  result.add(lj[i].multiply(yj[i]));
        }
        System.out.println(result);
        result = result.setScale(0, RoundingMode.HALF_EVEN);
        System.out.println(result);
    }
}
